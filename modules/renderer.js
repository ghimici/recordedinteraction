const dateFormat = require('dateformat')
const electron = require('electron')
const { ipcRenderer } = electron
const ul = document.querySelector('ul')
const stats_div = document.querySelector('#stats')
const fs = require('fs')
const unzipper = require('unzipper')
const request = require('request')

let data

ipcRenderer.on('stats', function () {
    if (data.records.length == 0) {
        return
    }
    let my_records = get_records()
    let frequencies = calculate_frequencies(my_records)
    let { max_name, max_value } = calculate_longest_sequence(my_records)
    let { min, max, mean } = calculate_time_delay()
    let total_interaction_time = calculate_total_interaction_time()

    console.log({ frequencies, min, max, mean, max_name, max_value })

    stats_div.innerHTML = `<p>Time dalays:</br> MIN = ${min}</br> MAX = ${max}</br> MEAN = ${mean}</p>`
    stats_div.innerHTML += `<p>Longest sequence:</br> ${max_name}: ${max_value}\n</p>`
    stats_div.innerHTML += `<p>Frequency per event type:</br>`
    for (let [key, value] of Object.entries(frequencies)) {
        stats_div.innerHTML += `${key}: ${value}</br>`
    }
    stats_div.innerHTML += `</p>`
    stats_div.style.display = 'block'
})

ipcRenderer.on('clear', function () {
    data.records.splice(0)
    ul.innerHTML = ''
})

function create_item(item, index) {
    let { name, type, date } = item
    let li = document.createElement("li")

    let itemText = document.createElement("span")
    itemText.innerText = `Item: ${name} - ${type} - ${date}`
    if (index % 2) {
        itemText.style.backgroundColor = "WhiteSmoke"
    }

    var btnDelete = document.createElement("button")
    btnDelete.innerHTML = "delete"
    btnDelete.addEventListener("click", function (event) {
        remove_record(this.className)
        load_ui()
    })

    let div = document.createElement("div")
    div.appendChild(itemText)
    li.appendChild(div)
    li.appendChild(btnDelete)
    ul.appendChild(li)
}

function get_records() {
    // create object with the useful data 
    return data.records.map(function (currentObject) {
        return {
            name: currentObject.setup.computedRole,
            type: currentObject.event.type,
            date: dateFormat(new Date(currentObject.time), "yyyy-mm-dd hh:MM:ss")
        }
    })
}

function calculate_time_delay() {
    let index = 0, sum = 0
    let min, max, mean
    // initialize min and max with the first difference
    min = max = Math.abs(data.records[index].time - data.records[index + 1].time)

    for (index = 0; index < data.records.length - 1; index++) {
        // get current difference
        let current = Math.abs(data.records[index].time - data.records[index + 1].time)
        // add it to the sum for mean
        sum += current
        // update min/max
        if (current > max) {
            max = current
        } else if (current < min) {
            min = current
        }
    }
    mean = sum / (data.records.length - 1)
    return { min, max, mean }
}

function calculate_longest_sequence(my_records) {
    // filter out focus events
    let records_without_focus = my_records.filter((el) => el.type !== "focus")

    let index = 0, curr = 1, max_value = 0, max_name = ''

    for (index = 0; index < records_without_focus.length - 1; index++) {
        // Check if the elements are same 
        if (records_without_focus[index].type == records_without_focus[index + 1].type) {
            curr = curr + 1
        } else {
            // Reinitialize current 
            curr = 1
        }
        // Compare the maximum 
        if (max_value < curr) {
            max_value = curr
            max_name = records_without_focus[index].type
        }
    }
    return { max_name, max_value }
}

function calculate_frequencies(records) {
    return records.reduce((acc, el) => {
        if (acc[el.type] || acc[el.type] === 0) {
            acc[el.type]++
        } else {
            acc[el.type] = 0
        }
        return acc
    }, {})
}

function calculate_total_interaction_time() {
    return 0
}

ipcRenderer.on('save', function () {
    // write json
    var element = document.createElement("a")
    element.setAttribute(
        "href",
        "data:text/json;charset=utf-8," +
        encodeURIComponent(JSON.stringify(data.records))
    );
    element.setAttribute("download", "events.json")
    element.style.display = "none"
    document.body.appendChild(element)
    element.click()
    document.body.removeChild(element)
});

function remove_record(i = Number(x)) {
    data.records.splice(i, 1)
}

function load_data() {
    return new Promise(function (resolve, reject) {
        // download archive and read json
        request.get({
            url: 'https://www.dropbox.com/s/rwbflb2v84c1173/task.recording.json.zip?dl=1'
        })
            .pipe(fs.createWriteStream('input/archive.zip')).on('close', function () {
                console.log('Response ZIP File written!')

                fs.createReadStream('input/archive.zip')
                    .pipe(unzipper.Parse()).on('entry', async function (entry) {

                        if (entry.path === 'task.recording.json') {
                            console.log('File has been found!')

                            const content = await entry.buffer();
                            data = JSON.parse(content.toString())
                            console.log(data)
                            resolve();
                        } else {
                            entry.autodrain();
                        }
                    });
            })
    })
}

function load_ui() {
    ul.innerHTML = ''

    // load json to ui
    let my_records = get_records()
    my_records.forEach(create_item)
};

// call load json
load_data().then(load_ui)
