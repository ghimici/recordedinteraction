# Electron RecordedInteraction

### Version
1.0.0

### Installation

Install the dependencies

```sh
$ npm install
```

### Run

```sh
$ npm start
```