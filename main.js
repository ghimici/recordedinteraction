const electron = require('electron')


const { app, BrowserWindow, Menu, ipcMain } = electron

let mainWindow

// Listen for app to be ready
app.on('ready', function () {
    // Create new window
    mainWindow = new BrowserWindow({
        webPreferences: {
            nodeIntegration: true // sa pot folosi in renderer node
        }
    })
    // Load html in window
    mainWindow.loadURL(`file://${__dirname}/index.html`);
    // Quit app when closed
    mainWindow.on('closed', function () {
        app.quit()
    })

    // Build menu from template
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate)
    // Insert menu
    Menu.setApplicationMenu(mainMenu)
})

// Create menu template
const mainMenuTemplate = [
    // Each object is a dropdown
    {
        label: 'File',
        submenu: [
            {
                label: 'Save JSON',
                click() {
                    mainWindow.webContents.send('save')
                }
            },
            {
                label: 'Show Stats',
                click() {
                    mainWindow.webContents.send('stats')
                }
            },
            {
                label: 'Clear Items',
                click() {
                    mainWindow.webContents.send('clear')
                }
            },
            {
                label: 'Quit',
                accelerator: 'Ctrl+Q',
                click() {
                    app.quit()
                }
            }
        ]
    }
]
mainMenuTemplate.push({
    label: 'Toggle DevTools',
    click(item, focusedWindow) {
        focusedWindow.toggleDevTools()

    }
})
